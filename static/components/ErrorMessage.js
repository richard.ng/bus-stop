import React from 'react'
import {
    Message
} from 'semantic-ui-react';

const ErrorMessage = () => (

    <
    Message warning >
    <
    Message.Header > Your input is invalid < /Message.Header> <
    Message.Content > Please enter a valid stop number (e.g.50848) or a valid bus route number (e.g.99) < /Message.Content> <
    /Message>
)

export default ErrorMessage
