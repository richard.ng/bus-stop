import React from 'react'
import { Header, Icon } from 'semantic-ui-react'

const BusHeader = () => (
    <Header as='h2' block>
        <Icon name='bus' />
        <Header.Content>
            Metro Vancouver Transit
        <Header.Subheader>Get upcoming scheduled bus times</Header.Subheader>
        </Header.Content>
    </Header>
)

export default BusHeader
