// App.jsx
import React from 'react';
import axios from 'axios';
import { Header, Icon, List, Button, Form, Segment, Message } from 'semantic-ui-react';
import BusHeader from '../components/BusHeader';
import ErrorMessage from '../components/ErrorMessage';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            userInput: ''
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleSubmit(event) {
        const options = {
            headers: { 'content-type': 'application/json' }
        };
        this.setState({ userInput: event.target.stop.value });

        axios.post('/stop', {
            number: event.target.stop.value
        }, options)
            .then(res => {
                if (res.data.length == 0 || res.data.length == undefined || res.data.length == 27) {
                    this.setState({ data: null });
                }
                else {
                    this.setState({ data: res.data });
                }
            })
            .catch(error => {
                console.log(error.response.data);

            })
    }

    handleClick(event) {
        this.setState({ data: [] });
        const options = {
            headers: { 'content-type': 'application/json' }
        };
        this.setState({ userInput: event });

        axios.post('/stop', {
            number: event
        }, options)
            .then(res => {
                this.setState({ data: res.data });
            })
            .catch(error => {
                console.log(error.response.data);
            })
    }

    render() {
        let cards;
        if (this.state.data == null) {
            cards = this.renderError();
        }
        else {
            if (this.state.userInput.length === 5) {
                cards = this.renderTimes();
            }
            else if (this.state.userInput.length > 0 && this.state.userInput.length < 4) {
                cards = this.renderDirections();
            }
        }

        return (
            <div className="ui segment">
                <BusHeader />
                <Form onSubmit={this.handleSubmit} >
                    <Form.Field>
                        <label>Bus Stop Number or Route num</label>
                        <input name='stop' placeholder='Stop or Route Number' />
                    </Form.Field>
                    <Button primary type='submit' onClick={() => this.setState({ data: [] })}>Search</Button>
                </Form >
                {cards}
            </div >
        )
    }

    renderError() {
        return (
            <ErrorMessage />
        )
    }

    renderDirections() {
        return (
            this.state.data.map((d, i) => {
                return (
                    <Segment inverted key={i}>
                        <List divided inverted relaxed>
                            <List.Item>
                                <List.Content>
                                    <List.Header>{d.route_number} {d.route_name}</List.Header>
                                    <List>
                                        {d.stops.map((s, j) => {
                                            return (<List.Item key={j} onClick={() => this.handleClick(s.number)} as='a'>
                                                {s.name}
                                            </List.Item>)
                                        })}
                                    </List>
                                </List.Content>
                            </List.Item>
                        </List>
                    </Segment>
                )
            })
        )
    }

    renderTimes() {
        return (
            this.state.data.map(function (b, i) {
                return (
                    <Segment inverted key={i}>
                        <List divided inverted relaxed>
                            <List.Item>
                                <List.Content>
                                    <List.Header>{b.route_number} {b.route_name}</List.Header>
                                    <List.Description>{b.stop_name}</List.Description>
                                    <List>
                                        {b.ScheduledLeaveTimes.map(function (t, j) {
                                            return (<List.Item key={j}>
                                                {t.time}
                                            </List.Item>)
                                        })}
                                    </List>
                                </List.Content>
                            </List.Item>
                        </List>
                    </Segment>
                )
            })
        )
    }
}

export default App;
