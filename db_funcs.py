import psycopg2
import getpass

connection = psycopg2.connect(
    database="bus", user="admin", password="1234", host="127.0.0.1", port="5432")
cursor = connection.cursor()

USERNAME = input("Enter Username: ").strip()
PASSWORD = getpass.getpass("Enter Password: ")

def check_user_exists():
    select = "select * from users"

    cursor.execute(select)
    records = cursor.fetchall()

    for c,v in enumerate(records):
        if USERNAME in records[c]:
            return True

def create_user(USERNAME, PASSWORD):
    try:
        if check_user_exists() == True:
            print("User already exists")
        else:
            insert = """ INSERT INTO users (USERNAME, PASSWORD) VALUES (%s, %s)"""

            cursor.execute(insert, (USERNAME, PASSWORD))
            connection.commit()

            count = cursor.rowcount
            print(count, "Record inserted successfully into users table")

    except (Exception, psycopg2.Error) as error:
        if(connection):
            print("Failed to insert record into users table", error)

    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("Connection closed")