import os
import scrapy
import re
from scrapy.exceptions import CloseSpider
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor

try:
    os.remove("all_routes.json")
except:
    pass

print("Fetching data... Please wait")

class BusStops(scrapy.Item):
    route_number = scrapy.Field()
    route_name = scrapy.Field()
    direction = scrapy.Field()
    stops = scrapy.Field()

class busSpider(scrapy.Spider):
    name = "bus_routes"

    custom_settings = {
    'LOG_ENABLED': False,
    # comment out below to scrape ALL routes
    # 'CLOSESPIDER_PAGECOUNT': 5,
    # will take a long time!!
    'COOKIES_ENABLED': False,
    'DOWNLOAD_DELAY': 2,
    "FEED_FORMAT": "json",
    "FEED_URI": "all_routes.json"
    }

    start_urls = ['https://nb.translink.ca/Text/Route']

    def parse(self, response):
        route_url = response.css('.destList > a::attr(href)').extract()

        for link in route_url:
            url = 'https://nb.translink.ca' + link
            yield scrapy.Request(url, callback=self.parse2)

    def parse2(self, response):
        direction_url = response.css('a.direction.ui-link-inherit::attr(href)').extract()

        for link in direction_url:
            url = 'https://nb.translink.ca' + link
            yield scrapy.Request(url, callback=self.parse3)

    def parse3(self, response):
        names = response.css('.stop.ui-link-inherit::text').extract()
        numbers = response.css('a.stop.ui-link-inherit::attr(href)').extract()
        for i,v in enumerate(numbers):
            numbers[i] = re.search(r'\d{5,5}',v).group()
        list = []

        Bus = BusStops()
        Bus["route_number"] = response.css('.txtRouteTitle::text').extract_first().split(' ', 1)[0]
        Bus["route_name"] = response.css('.txtRouteTitle::text').extract_first().split(' ', 2)[2]
        Bus["direction"] = response.url
        for i,j in zip(names,numbers):
            list.append({"name": i, "number": j})
        Bus["stops"] = list
        
        yield Bus

runner = CrawlerRunner()
d = runner.crawl(busSpider)
d.addBoth(lambda _: reactor.stop())
reactor.run()
print("Data fetching complete.")