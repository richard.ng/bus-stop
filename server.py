from flask import Flask, render_template, request, jsonify
import bus_funcs
import json

app = Flask(__name__, static_folder="./static/dist",
            template_folder="./static")

with open('all_routes.json') as file:
    list_of_routes = json.load(file)

@app.route("/")
def index(): 
    return render_template("index.html")

@app.route("/stop", methods=['POST','GET'])
def stop():

    valid_night_bus = ['N8', 'N9', 'N10', 'N15', 'N17', 'N19', 'N20' 'N22', 'N24', 'N35']

    result=[]

    if request.method == 'POST': 
        data = request.get_json()
        number=data["number"].capitalize()
        # if input is a bus stop number has to have all digits
        if len(number) == 5 and number.isdigit(): 
            result=bus_funcs.get_stop_info(number)

        # if bus stop is a route 
        elif number in valid_night_bus or number.isdigit():  
            for route in list_of_routes: 
                if number == route['route_number']:
                    result.append(route)
            result = jsonify(result)
        
        else: 
            result = jsonify('Invalid bus or route number')

    return result

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
