# config.py contains api_token
import config
import json
import requests
import os
from scrapy.exceptions import CloseSpider
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor
from route import busSpider

# API token obtained from Translink after creating an account
api_token = config.api_token
api_url_base = 'https://api.translink.ca/rttiapi/v1/stops'

def find_stop_name(stop_num): 
    api_url = f'{api_url_base}/{stop_num}?apikey={api_token}'
    headers = {'accept': 'application/JSON'}

    response = requests.get(api_url, headers=headers)

    info = json.loads(response.text)

    return(info['OnStreet'] + ' AT ' + info['AtStreet'])

def get_stop_info(stop_num):
    api_url = f'{api_url_base}/{stop_num}/estimates?apikey={api_token}'
    headers = {'accept': 'application/JSON'}

    response = requests.get(api_url, headers=headers)

    info = json.loads(response.text)
    arr = []

    if 'Code' in info:
        print(info)
        return info

    else:
        for c,v in enumerate(info):
            dict = {"route_number": info[c]["RouteNo"],
                    "route_name": info[c]["RouteName"].strip(),
                    "stop_name": find_stop_name(stop_num),
                    "ScheduledLeaveTimes": []}

            schedule=[]

            for i,j in enumerate(info[c]["Schedules"]):
                schedule.append({"time": j["ExpectedLeaveTime"]})
    
            dict["ScheduledLeaveTimes"] = schedule
            arr.append(dict)

        print(arr)
        return(json.dumps(arr, indent=2))

def get_route_stops(num):
    try:
        os.remove("route.json")
    except:
        pass
    print("Fetching data... Please wait")
    runner = CrawlerRunner()
    d = runner.crawl(busSpider, domain=f"https://nb.translink.ca/text/route/{num}")
    d.addBoth(lambda _: reactor.stop())
    reactor.run()
    print("Data fetching complete.")