import psycopg2

conn = psycopg2.connect(database='bus', user='admin',
                        password='1234', host='127.0.0.1', port='5432')

print("Opened database successfully")

cur = conn.cursor()

cur.execute('''CREATE TABLE USERS
      (ID SERIAL NOT NULL,
      USERNAME        TEXT    NOT NULL,
      PASSWORD        CHAR(50) NOT NULL, 
      unique (USERNAME),
      PRIMARY KEY (ID));''')
print("Users table created successfully!")

conn.commit()
conn.close()
