busop: psql

psql:
	initdb bus_cluster
	pg_ctl -D bus_cluster start
	createdb bus
	createuser admin
	python3 create_users_db.py

connect:
	psql bus admin

clean: stop remove

stop:
	pg_ctl -D bus_cluster stop

remove:
	rm -r bus_cluster