# Use an alpine node image 
FROM node:12.7.0-alpine
# Copy current directory to app directory of container
COPY . /app/
# Change current working directory to app/static directory of container
WORKDIR /app/static/
# Build ./dist/*
RUN npm install && npm run build

# Use an alpine python3 image 
FROM python:3.7.4-alpine3.9
# Copy current directory to app directory of container
COPY . /app/
# Copy dist folder created from above to app directory of container
COPY --from=0 /app/static/dist/ /app/static/dist/
# Change current working directory to app directory of container
WORKDIR /app/
# Install required packages
RUN apk update && apk upgrade && pip install -r requirements.txt
# Expose port 8000 to outside of container 
EXPOSE 8000 
# Commands to run when container is launched
CMD ["python", "server.py"]